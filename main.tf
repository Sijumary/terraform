provider "aws" {
    region = "ap-south-1"
    access_key = "AKIAQSKJTPHGNABQS546"
    secret_key = "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"
}

# export AWS_ACCESS_KEY_ID="AKIAQSKJTPHGNABQS546"
# export AWS_SECRET_ACCESS_KEY="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"
# export AWS_DEFAULT_REGION="us-west-2"

variable vpc_cidr_block {}
variable subnet_cidr_block {}
variable avail_zone {}
variable env_prefix {}
variable my_ip {}

resource "aws_vpc" "myapp-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
     Name: "${var.env_prefix}-vpc"
    }
}
resource "aws_subnet" "myapp-subnet-1" {
    vpc_id = aws_vpc.myapp-vpc.id
    #cidr_block = "10.0.0.0/24"
    cidr_block = var.subnet_cidr_block
    availability_zone = var.avail_zone 
    tags = {
     Name: "${var.env_prefix}-subnet-1"
    }
}
# resource "aws_route_table" "myapp-route-table" {
#     vpc_id =aws_vpc.myapp-vpc.id
#     route {
#         cidr_block = "0.0.0.0/0"
#         gateway_id = aws_internet_gateway.myapp-igw.id
#     }
#     tags = {
#      Name: "${var.env_prefix}-rtb"
#     }
  
# }
resource "aws_internet_gateway" "myapp-igw" {
    vpc_id =aws_vpc.myapp-vpc.id
    tags = {
     Name: "${var.env_prefix}-igw"
    }

}
resource "aws_default_route_table" "main-rtb"{
    default_route_table_id = aws_vpc.myapp-vpc.default_route_table_id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.myapp-igw.id
    }
    tags = {
        Name: "${var.env_prefix}-main-rtb"
    }
}
resource "aws_security_group" "myapp-sg"{
    name = "myapp-sg"
    vpc_id = aws_vpc.myapp-vpc.id

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.my_ip]
    }
    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks =["0.0.0.0/0"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks =["0.0.0.0/0"]
        prefix_list_ids = []
    }
    tags = {
        Name: "${var.env_prefix}-sg"
    }
}
# resource "aws_route_table_association" "a-rtb-subnet" {
#     subnet_id = aws_subnet.myapp-subnet-1.id
#     route_table_id = aws_route_table.myapp-route-table.id
# }

# output "dev-vpd-id" {
#   value = aws_vpc.development-vpc.id
# }

# output "dev-subnet-id" {
#   value = aws_subnet.dev-subnet-1.id
# }

#  data "aws_vpc" "existing_vpc" {
#         default = true
# }
# resource "aws_subnet" "dev-subnet-2" {
#     vpc_id = data.aws_vpc.existing_vpc.id
#    cidr_block ="172.31.48.0/20"
#     availability_zone = "ap-south-1a" 
#     tags = {
#      Name ="subnet-2-default"
#  }
# } 